# Livespace

To repozytorium to rozwiązania przesłanych zadań.

Podszedłem do tego tak, że same zadania zostawiłem nieruszone dodając jedynie komentarze, trochę na wzór code review.

Same rozwiązania są w katalogu `solutions`. By ułatwić sobie życie ubrałem całość w projekt oparty na CRA i popisałem proste testy jednostkowe.
Kod rozwiązań dostarczam w TypeScripcie.
Dzięki temu łatwo zweryfikować poprawność kodu.

Aby sprawdzić rozwiązania najlepiej uruchomić:

* `yarn start` - by zobaczyć wizualną stronę tychże
* `yarn test` - by uruchomić unit testy

**Uwagi**
Większość problemów z kodem (a także pytań postawionych w treściach zadań) jest do zidentyfikowania przez choćby domyślną konfigurację eslinta lub dodanie TypeScript.
Nie mając precyzyjnych wymogów "biznesowych" co do poszczególnych zadań, nie porywałem się za bardzo na radykalne refaktoryzacje czy jakieś ruchy architektoniczne.
Gdzieniegdzie dodałem odpowiednie komentarze zahaczające o tematy architektoniczne właśnie. 
Warto również nadmienić, że w większości przypadków, kod produkcyjny mający rozwiązać podobne problemu, wyglądałby cokolwiek inaczej. Wiele by zależało właśnie od architektury całej aplikacji, źródeł danych, podziału na domeny itp.
  
