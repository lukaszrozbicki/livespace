import React, { FunctionComponent, PureComponent } from 'react';

interface ListElementProps {
  number: number;
  content: string;
}

/**
 * Ubrałem ListElement w `React.memo` by zmemoizować ten komponent i nie renderować go bez potrzeby.
 */
const ListElement: FunctionComponent<ListElementProps> = React.memo(props => {
  console.log('list element render');

  return (
    <li>{props.number}. {props.content}</li>
  )
});

interface Element {
  content: string;
  index: number;
}

interface FourthState {
  elements: Array<string>;
  transformedElements: Element[];
}

/**
 * Ten komponent można by także zrobić funkcyjnym. Wtedy transformacje danych można by zrobić
 * bardziej deklaratywnie, nie uzależniając ich od stanu zamontowania komponentu (bo czemu miało
 * by to wpływać na dane?) ale od samych danych (dzięki useEffect z `elements` jako zależność).
 */
export class Fourth extends PureComponent<{}, FourthState> {
  state = {
    elements: (
      new Array(100)
    ).fill(1).map<string>(() => `I'm a list element! ${Math.floor(Math.random() * 1000)}`),
    /**
     * Dodalem do stanu listę elementów po transformacji. Sama transformacja dzieje się zaraz po
     * zamontowaniu komponentu, ale przed inicjalnym pokazaniem komponentu użytkownikowi.
     */
    transformedElements: [],
  };

  /**
   * Jak wspomniano wyżej - tłumaczenie danych wejściowych na formę uzywaną przez UI dzieje się tu.
   * To też właściwie nie jest idealne miejsce, ale na potrzeby tego zadania niech tak zostanie.
   * Jakkolwiek to normalna sytuacja, że dane wchodzą do aplikacji w postaci innej niż ta, która
   * jest wykorzystywana do faktycznej prezentacji danych, tak powinny być jasno oddzielone od
   * siebie warstwy transportu i prezentacji. Idealnie by było, jakby UI (w tym wypadku
   * komponent listy) nie miał pojęcia o tym, jak wyglądają dane z zewnątrz. Samo mapowanie na
   * inną postać powinno mieć miejsce dużo niżej ale też nie w samej warstwie transportu, by jej
   * nie zaburzyć.
   */
  componentDidMount(): void {
    const transformedElements = this.state.elements.reduce<Element[]>(
      (accumulatedData: Element[], value: string, index: number) => {
        if (value) {
          accumulatedData.push({
            index: index,
            content: value,
          })
        }

        return accumulatedData;
      },
      [],
    );

    this.setState({
      transformedElements,
    })
  }

  handleClick = () => {
    /**
     * Click handler robi teraz prostą operację usunięcia pierwszego elementu z listy elementów
     * po transformacji. Dzięki temu, nie ma ryzyka zmiany indexu elementu, a co za tym idzie,
     * można dzięki temu użyć memoizacji komponentu ListElement.
     */
    this.setState((prevState) => {
      const transformedElements = [...prevState.transformedElements];

      transformedElements.shift();

      return {
        transformedElements,
      }
    });
  };

  render() {
    const { transformedElements } = this.state;
    console.log('Fourth render');

    return (
      <div>
        <button onClick={this.handleClick}>Remove first list element</button>
        <ul style={{ listStyle: 'none' }}>
          {transformedElements.map((value: Element) => (
            <ListElement key={value.index} content={value.content} number={value.index + 1} />
          ))}
        </ul>
      </div>
    )
  }
}
