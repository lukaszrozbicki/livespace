import React, { Component, FunctionComponent, useCallback, useEffect, useState } from 'react'

interface CounterProps {
  add: number;
  count: number;
}

const Counter: FunctionComponent<CounterProps> = ({ add, count }) => (
  <p>{`${add} plus current count is ${add + count}`}</p>
);

interface SecondState {
  count: number;
}

export class Second extends Component<{}, SecondState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      count: 0,
    }
  }

  handleClick = () => {
    this.setState(
      (prevState) => ({
        count: prevState.count + 1,
      }),
      () => {
        console.log(`Second newCount = ${100 + this.state.count}`);
      }
    );
  };

  render() {
    return (
      <div>
        <button onClick={this.handleClick}>count up</button>
        <Counter add={100} count={this.state.count}/>
      </div>
    );
  }
}

interface Counter2Props {
  add: number;
  baseCounterValue: number;
}

/**
 * Wyodrębniłem tutaj logikę obliczania nowego licznika do custom hooka.
 * W ten sposóļ komponent `Counter` nie zawiera żadnej bezpośredniej implementacji logiki
 * (potencjalnie) biznesowej.
 * Odpowiada jedynie za uruchomienie mechanizmu obliczania licznika i wyświetlenie danych (nowej
 * wartości licznika).
 *
 * W zależności od potrzeb, można by całkowicie wynieść stan i logikę licznika do contextu,
 * przygotować odpowiedni provider, który by eksponował API licznika (np. counterValue,
 * updateCounter) a same komponenty odpowiedzialne za zmianę licznika i wyświetlanie jego stanu
 * były by bardzo proste. Przy takim oddzieleniu prezentacji od logiki łatwo można reużywać
 * logiki licznika, zmieniać ją itp bez szkody dla samych komponentów. One pozostają bez zmian.
 */
const useCounter = (add: number, baseCounterValue: number): number => {
  const [counter, setCounter] = useState<number>(baseCounterValue);

  useEffect(
    () => {
      const newCounter = add + baseCounterValue;

      setCounter(newCounter);
      console.log(`SecondFN newCounter = ${newCounter}`);
    },
    [add, baseCounterValue],
  );

  return counter;
};

const Counter2: FunctionComponent<Counter2Props> = ({ add, baseCounterValue }) => {
  const counter = useCounter(add, baseCounterValue);

  return (
    <p>{`${add} plus current count is ${counter}`}</p>
  )
};

interface SecondFCProps {
  baseCounterValue: number;
}

export const SecondFC: FunctionComponent<SecondFCProps> = ({ baseCounterValue }) => {
  const [count, setCount] = useState<number>(0);
  const handleClick = useCallback(
    () => {
      const newCount = count + 1;

      setCount(newCount);
    },
    [count],
  );

  return (
    <div>
      <button onClick={handleClick}>count up</button>
      <Counter2 add={count} baseCounterValue={baseCounterValue}/>
    </div>
  );
};
