import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Third } from './third';

jest.useFakeTimers();

describe('<Third />', () => {
  it('renders "Search for" no text if input is not filled', () => {
    const { container } = render(<Third />);

    const searchFor = container.querySelector('p');

    expect(searchFor).not.toBeInTheDocument();
  });

  it('renders "Search for" text with search query when input is filled', () => {
    const { container, getByText } = render(<Third />);

    const input = container.querySelector('input') as HTMLInputElement;

    fireEvent.change(input, { target: { value: 'test' }});
    jest.runAllTimers();

    const searchFor = getByText('Search for: test');

    expect(searchFor).toBeInTheDocument()
  });
});
