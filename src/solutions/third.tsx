import React, { ChangeEvent, Component } from 'react'

export class Third extends Component {
  state = { search: '' };
  timeout: number = 0;

  handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const search = event.target.value;

    clearTimeout(this.timeout);

    this.timeout = window.setTimeout(() => {
      this.setState({
        search,
      })
    }, 250);
  };

  render() {
    return (
      <div>
        <input type="text" onChange={this.handleChange} />
        {this.state.search ? <p>Search for: {this.state.search}</p> : null}
      </div>
    )
  }
}
