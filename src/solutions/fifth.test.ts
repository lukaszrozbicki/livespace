import { createApp, initialState, State } from './fifth';

describe('createApp', () => {
  it('handles SET_VISIBILITY_FILTER action', () => {
    const result = createApp(initialState, {
      type: 'SET_VISIBILITY_FILTER',
      data: {
        filter: 'display',
      },
    });
    const expected: State = {
      ...initialState,
      visibilityFilter: 'display',
    };

    expect(result).toEqual(expected);
  });

  it('handles CHANGE_PRODUCT_PRICE action', () => {
    const result = createApp(initialState, {
      type: 'CHANGE_PRODUCT_PRICE',
      data: {
        id: 305,
        newPrice: 69,
      },
    });
    const expected: State = {
      ...initialState,
      products: [
        {
          id: 305,
          price: 69,
          lastSold: '2020-02-12T13:19:15.784Z',
        },
        {
          id: 308,
          price: 500,
          lastSold: '2020-02-14T11:12:18.113Z',
        },
        {
          id: 309,
          price: 299,
          lastSold: '2020-02-14T14:32:06.361Z',
        },
      ],
    };

    expect(result).toEqual(expected);
  });

  it(
    'does not affect state if there is no product of passed id from CHANGE_PRODUCT_PRICE action',
    () => {
      const result = createApp(initialState, {
        type: 'CHANGE_PRODUCT_PRICE',
        data: {
          id: 476,
          newPrice: 69,
        },
      });

      expect(result).toEqual(initialState);
    },
  );

  it('handles UPDATE_LAST_SOLD action', () => {
    const result = createApp(initialState, {
      type: 'UPDATE_LAST_SOLD',
      data: {
        id: 305,
      },
    });
    const expected: State = {
      ...initialState,
    };

    expect(result).toEqual(expected);
  });

  it(
    'does not affect state if there is no product of id passed from  UPDATE_LAST_SOLD action',
    () => {
      const result = createApp(initialState, {
        type: 'UPDATE_LAST_SOLD',
        data: {
          id: 305,
        },
      });
      const expected: State = {
        ...initialState,
      };

      expect(result).toEqual(expected);
    },
  );
});
