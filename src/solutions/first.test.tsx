import React from 'react';
import { render } from "@testing-library/react";
import { First } from './first';

describe('<First />', () => {
  it('renders with Anonymous instead of name', () => {
    const { getByText } = render(<First />);

    expect(getByText('Hello Anonymous')).toBeInTheDocument();
  });

  it('renders with a name passed as a prop', () => {
    const { getByText } = render(<First name="Test" />);

    expect(getByText('Hello Test')).toBeInTheDocument();
  });
});
