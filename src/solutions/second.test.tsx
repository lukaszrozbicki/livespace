import React from 'react';
import { render, act } from '@testing-library/react';
import { Second, SecondFC } from './second';

describe('<Second />', () => {
  it('renders correctly', () => {
    const { container } = render(<Second />);

    expect(container.childElementCount).toBe(1);
  });

  it('renders counter properly', () => {
    const { getByText } = render(<Second />);

    const counter = getByText('100 plus current count is 100');

    expect(counter).toBeInTheDocument()
  });

  it('modifies counter when button is clicked', () => {
    const { getByText } =render(<Second />);

    const button = getByText('count up') as HTMLButtonElement;

    act(() => {
      button.click();
    });

    const counter = getByText('100 plus current count is 101');

    expect(counter).toBeInTheDocument()
  });
});


describe('<SecondFC />', () => {
  it('renders correctly', () => {
    const { container } = render(<SecondFC baseCounterValue={100} />);

    expect(container.childElementCount).toBe(1);
  });

  it('renders counter properly', () => {
    const { getByText } = render(<SecondFC baseCounterValue={100}  />);

    const counter = getByText('0 plus current count is 100');

    expect(counter).toBeInTheDocument()
  });

  it('modifies counter when button is clicked', () => {
    const { getByText } =render(<SecondFC baseCounterValue={100}  />);

    const button = getByText('count up') as HTMLButtonElement;

    act(() => {
      button.click();
    });

    const counter = getByText('1 plus current count is 101');

    expect(counter).toBeInTheDocument()
  });
});
