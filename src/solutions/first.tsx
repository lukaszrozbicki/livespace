import React, { FunctionComponent } from 'react'

interface AppProps {
  name?: string;
}

export const First: FunctionComponent<AppProps> = ({ name = 'Anonymous' }) => (
  <div>
    <input type="text" />
    <p>Hello {name}</p>
  </div>
);

