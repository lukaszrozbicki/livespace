interface Product {
  id: number;
  lastSold: string;
  price: number;
}

type VisibilityFilter = 'none' | 'display';

export interface State {
  visibilityFilter: VisibilityFilter;
  products: Product[];
}

type SetVisibilityFilter = {
  data: {
    filter: VisibilityFilter;
  };
  type: 'SET_VISIBILITY_FILTER';
}

type ChangeProductPrice = {
  data: {
    id: number;
    newPrice: number;
  };
  type: 'CHANGE_PRODUCT_PRICE';
}

type UpdateLastSold = {
  data: {
    id: number;
  };
  type: 'UPDATE_LAST_SOLD';
}

type Actions = SetVisibilityFilter | ChangeProductPrice | UpdateLastSold;

export const initialState: State = {
  visibilityFilter: 'none',
  products: [
    {
      id: 305,
      price: 100,
      lastSold: '2020-02-12T13:19:15.784Z',
    },
    {
      id: 308,
      price: 500,
      lastSold: '2020-02-14T11:12:18.113Z',
    },
    {
      id: 309,
      price: 299,
      lastSold: '2020-02-14T14:32:06.361Z',
    },
  ],
};

export function createApp(state: State = initialState, action: Actions) {
  switch (action.type) {
    case 'SET_VISIBILITY_FILTER': {
      return {
        ...state,
        visibilityFilter: action.data.filter,
      };
    }
    case 'CHANGE_PRODUCT_PRICE': {
      const productIndex = state.products.findIndex(({ id }) => action.data.id === id);

      if (productIndex === -1) {
        return state;
      }

      const newProducts = [ ...state.products ];

      newProducts.splice(
        productIndex,
        1,
        {
          ...state.products[productIndex],
          price: action.data.newPrice,
        },
      );

      return {
        ...state,
        products: newProducts,
      }
    }

    case 'UPDATE_LAST_SOLD': {
      const stateCopy = {
        ...state,
        products: [ ...state.products ],
      };
      const productIndex = stateCopy.products.findIndex(({ id }) => action.data.id === id);

      if (!productIndex) {
        return state;
      }

      stateCopy.products.splice(
        productIndex,
        1,
        {
          ...stateCopy.products[productIndex],
          lastSold: (
            new Date()
          ).toISOString(),
        },
      );

      return stateCopy;
    }
    default:
      return state;
  }
}
