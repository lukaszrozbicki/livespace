import React from 'react';
import ReactDOM from 'react-dom';
import { First } from './solutions/first';
import { Second, SecondFC } from './solutions/second';
import { Third } from './solutions/third';
import { Fourth } from './solutions/fourth';

ReactDOM.render(
  (
    <div>
      <h2>First</h2>
      <First name="Test" />
      <h2>Second</h2>
      <Second />
      <h2>SecondFC</h2>
      <SecondFC baseCounterValue={100} />
      <h2>Third</h2>
      <Third />
      <h2>Fourth</h2>
      <Fourth />
      <h2>Fifth</h2>
      <p>Check unit tests.</p>
    </div>
  ),
  document.getElementById('root'),
);
