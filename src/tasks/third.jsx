/*
    Poniższy komponent wygląda, jakby miał działać, ale... nie działa.
    Jak można go naprawić? Dlaczego nie działa?
*/

import React, { Component } from 'react'

class App extends React.Component {
    state = { search: '' }
    handleChange = event => {
        clearTimeout(this.timeout);
        /**
         * Problemem jest tu użycie setTimeout. A właściwie to, jak działają syntetyczne eventy// Reacta. Eventy
         * syntetyczne są "poolowane". Znaczy to, że po wykonaniu się callbacka z event listenera, obiekt eventu
         * jest czyszczony w celu ponownego jego użycia. Zatem próba odczytania wartości `value` spotka się z
         * odczytem tej wartości z nulla.
         * Można tego uniknąć na kilka sposobów.
         * - można wyciągnąć sobie tę wartość wcześniej, przed setTimeoutem
         * - można utrzymać obiekt eventu używając metody `event.persist()` - wtedy zostanie on (a bardziej
         * referencja do niego) zachowany w pamięci oraz usunięty z puli eventów.
         *
         * Osobiście sugerowałbym opcję pierwszą. Po co sobie zaśmiecać pamięć?
         */
        this.timeout = setTimeout(() => {
          this.setState({
            search: event.target.value
          })
        }, 250);
      }
    render() {
        return (
            <div>
                <input type="text" onChange={this.handleChange} />
                {this.state.search ? <p>Search for: {this.state.search}</p> : null}
            </div>
        )
    }
}
