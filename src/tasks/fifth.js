/*
    W reducerze createApp przy obsłudze każdej z akcji w kodzie jest jakiś problem.
    Jak można poprawić każdy z nich? Dlaczego z ich kodem jest coś nie tak?
*/

const initialState = {
    visibilityFilter: 'none',
    /**
     * Zastanawiam się nad tym, czy nie lepiej by było tutaj mieć mapę zamiast listy.
     * Operacje na produktach by były dużo prostsze, a samo użycie na UI też by niei było skomplikowane. Plus wtedy
     * dane na UI można by zmemoizować bardzo łatwo (np. za pomocą reselecta).
     * Ewentualnie można by pokusić się na normalizację stanu, ale nie wiem czy nie byłaby to sztuka dla sztuki
     * tylko po to, by zrobić ze stanu aplikacji coś a'la baza danych.
     */
    products: [
        {
            id: 305,
            price: 100,
            lastSold: '2020-02-12T13:19:15.784Z'
        },
        {
            id: 308,
            price: 500,
            lastSold: '2020-02-14T11:12:18.113Z'
        },
        {
            id: 309,
            price: 299,
            lastSold: '2020-02-14T14:32:06.361Z'
        },
    ]
}

function createApp(state = initialState, action) {
    switch(action.type){
        case SET_VISIBILITY_FILTER:
            /**
             * Ta operacja zwraca nowy obiekt, a nie mutuje/rozszerza `state`. Dlatego też dla akcji
             * SET_VISIBILITY_FILTER zwracany jest pierwotny stan.
             * W tym wypadku wynik Object.assing trzeba by zapisać do nowej zmiennej/stałej albo bezpośrednio zwrócić.
             * Równie dobrze można użyć spread operator i wykonać operację:
             * return {
             *   ...state,
             *   visibilityFilter: action.data.filter,
             * }
             */
            Object.assign({}, state, {
                visibilityFilter: action.data.filter
            })
            return state

        case CHANGE_PRODUCT_PRICE:
            /**
             * Tu jet problem dwojaki. Przede wszystkim jest próba odczytu produktu z tablicy po indeksie o wartości
             * `id` z payloadu akcji. No to może się udać, jesli jest dużo produktów, ale szanse są bardzo mało, że
             * trafimy akurat na ten wyamgany produkt ;) A nawet jeśli `products` w storze było by mapą, to i tak
             * problem byłby taki, że może nie być produktu o `id` z payloadu akcji. Trzeba by dodać sprawdzanie,
             * czy istnieje dany produkt i dopiero jeśli tak, to robić ustawienie ceny.
             *
             * Dodatkowo klucz `products` nie jest kopiowany, ale jest używany po referencji. Czyli mutowany. Zatem
             * idealnie by było zrobić też kopię `products`.
             */
            const stateCopy = {...state}
            stateCopy.products[action.data.id].price = action.data.newPrice
            return stateCopy

        case UPDATE_LAST_SOLD:
            /**
             * Tutaj jest podobny problem, jak wyżej - wyciągnięcie produktu po id.
             */
            const stateCopy = {
                ...state,
                products: [...state.products]
            }
            /**
             * Tak po prawdzie to fajnie by było, jakby `lastSold` przychodziło z zewnątrz. To
             * wygląda na dość biznesową wartość. Ustawianie tego po stronie klienta (czas
             * przeglądarki) może być dość tricky w przypadku, gdy dochodzi do produktu, który
             * działa w różnych strefach czasowych. Lepiej by było ustawiać to na serwerze,
             * wtedy dla każdej transkacji data by była serwerowa. Obsługa takich dat/timestamĻw
             * będzie dużo prostsza.
             */
            stateCopy.products[action.data.id].lastSold = (new Date()).toISOString()
            return stateCopy

        default:
            return state
    }
}
