/*
    Ten komponent nie działa 😱 Jak trzeba go naprawić? Dlaczego nie działał?
    Co w tym kompnencie można zmienić, żeby był ładniej/sensowniej napisany?
*/

import React, { Component } from 'react'

/**
 * Komponent nie musi być klasowy. Można go spokojnie zamienić na komponent funkcyjny (będący czystą funkcją).
 */
class App extends Component {
    constructor(props) {
        /**
         * Brakuje przekazania propsów do konstruktora nadrzędnej klasy. Bez tego nie można odwołać się do
         * this.props (tzn. można, ale zawsze będzie to nam zwracać `undefined`).
         */
        super();

        /**
         * Ten stan jest właściwie nie potrzebny w obecnej formie komponentu. Na danych z propsów nie wykonujemy
         * żadnych operacji, więc nie ma sensu komplikować tego komponentu. "Anonymous" jako domyślna wartość `name`
         * może być ustawiony jako default dla propa `name` w kompoonencie funkcyjnym.
         */
        this.state = {
            name: this.props.name || 'Anonymous'
        }
    }

    render() {
        return (
            <div>
                <p>Hello {this.state.name}</p>
            </div>
        );
    }
}
