/*
    Poniższy kod się nie uruchamia i ma w sobie kilka błędów. Jak go naprawić? Co można zmienić w tym kodzie?
*/

import React, { Component } from 'react'

const Counter = ({add, count}) => (<li>{`${add} plus current count is ${add + count}`}</li>)

/**
 * `Component` jest zaimportowane ale nie użyte. Zapis `React.Component` jest niepotrzebny w tym wypadku.
 * Ogólnie, podobnie jak w przypadku First, to też można by zapisać jako komponent funkcyjny.
 */
class App extends React.Component {
    // bardzo niefortunnie nazwany argument z propsami. Zaciemnia to kod.
    constructor(React) {
        super(React);
        this.state = {
            count: 0
        }
    }

    /**
     *  Metoda `handleClick` tak zadeklarowana nie będzie miała dostępu do kontektu komponentu. Trzeba by ją albo
     *  zbindowac w miejscu użycia, w JSX (bardzo fuj), albo w konstruktorze (tylko trochę fuj, bo faktyczne
     *  deklaracje metod są oddzielone wizualnie od miejsca ich zbindowania, nie jest to jakieś eleganckie do analizu),
     *  albo użyć arrowfunction, która ładnie sobie kontekst "pod spodem" podepnie (najlepsza opcja, pomimo pewnych
     *  głosów mówiących o wpływie na wydajność... ale dopóki nikt nie pokaże konkretnych danych, to nie ma co się
     *  przejmować taką przedwczesną mikro-optymalizacją).
     */
    handleClick() {
        /**
         *  Stan nie powinien być zmieniany w ten sposób. NIe powinien w ogóle być mutowany. Należy tu użyć
         *  metody setState().
         *  W celu zalogowania aktualnego stanu do konsoli, można wywołać console.log w callbacku do setState.
         */
        this.state.count = this.state.count + 1;
        /**
         * Te `100` to taki brzydki magic-number :( Do tego uzywany w kilku miejscach. Można by to wyrzucić właśnie
         * do propa jako `baseCounterValue` czy coś (jako, że to wyglą∂a mi jako bazowa wartość countera, a stan
         * komponentu odpowiada za składnik dodawany do tej bazowej wartości).
         */
        console.log(100 + this.state.count); // Chcemy, aby ten console log logował bięzacą wartość (tę samą, którą wyświetla Counter)
    }

    render() {
        return (
            <div>
                <button onClick={this.handleClick()}>count up</button>
                {/* Typ `add` się nie zgadza. Jest string, a powinien być number. Ogólnie `add` mogło by być */}
                {/* przekazywane jako prop do komponentu-rodzica. Zmieniłbym też propsy miejscami - `add` by brał */}
                {/* stan komponentu-rodzica, a `count` by odpowiadał za bazową wartość licznika.*/}
                <Counter add="100" count={this.state.count}/>
            </div>
        );
    }
}
